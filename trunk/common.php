<?php

function includeScript($file_path)
{
	require_once dirname(__FILE__) . '/' . $file_path;
}

function __autoload($class_name) {
	includeScript('includes/' . $class_name . '.class.php');
}

function escapeJSON($input)
{
	return addcslashes($input, '\\"\'');
}

includeScript('config.php');

?>
